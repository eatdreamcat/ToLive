/**
 * 游戏単例对象
 * 用来管理游戏中的各种数据
 */ 
var Game = {
	
	objects  : [], // 游戏中的对象信息（树木石头等...）
	monsters : null, // 游戏中的怪物
	rockArray : [], // 岩石层矩阵
	grassArray : [], // 草底层矩阵
	viewWidth  : null, // 视角宽度
	viewHeight : null, // 视角高度
	winSize    : null, // 窗口大小
	drawRect   : null, // 绘制窗口
	playerPos  : null, // 主角生成坐标
	gameScene  : null, // 游戏场景对象
	//  初始化
	init : function (scene) {
		
		this.gameScene = scene;
	},
	
	// 初始化所有数据
	initAllData : function() {
	
		var startx, starty;

        // 初始化尺寸数据
		this.winSize = cc.director.getWinSize();
		this.viewWidth = Math.ceil(this.winSize.width / tileDesignSize.width);
		this.viewHeight = Math.ceil(this.winSize.height / tileDesignSize.height);

		// 初始化地图数据
		this.initMap();
		this.generateMapData(mapTypeRate.rockRate, mapType.rock, mapType.water, this.rockArray);
		this.generateMapData(mapTypeRate.rockRate, mapType.grass, mapType.water, this.grassArray);

		// 初始化玩家坐标
		this.playerPos = this.initPlayerPos();
		cc.log("玩家初始坐标: " + JSON.stringify(this.playerPos));
		// 初始化绘制窗口
		startx = this.playerPos.x - Math.round(this.viewWidth / 2);
		starty = this.playerPos.y - Math.round(this.viewHeight / 2);
		this.drawRect = {
				startX : startx - 1,
				endX : this.viewWidth + startx + 2,
				startY : starty - 1,
				endY : this.viewHeight + starty,
		}
        
		// 初始化环境信息
		this.initObjectData();
		
	},
	
	// 获取地图中物品信息矩阵
	getObjectArray : function() {
	    return this.objects;	
	},
	
	// 获取随机pos
	getRandomPosition : function(i, j) {
		//parseInt(pos.x * this.tileSize.width), parseInt(pos.y * this.tileSize.height)
		var startX = i * tileDesignSize.width + tileDesignSize.width / 4;
		var startY = j * tileDesignSize.height + tileDesignSize.height / 4;
		return cc.p(startX + cc.random0To1()*tileDesignSize.width*0.5,
				startY + cc.random0To1()*tileDesignSize.height*0.5);
		
	},
	
	// 初始化地图中物品数据
	initObjectData : function() {
		
		var i, j, pos;
		
		for (i = 0; i < this.rockArray.length; i++) {
			for (j = 0; j < this.rockArray[i].length; j++ ) {
				
				// 初始化人头树（只生长在草地）
				if (this.grassArray[i][j] === mapType.grass) {
					if (cc.random0To1() < mapTypeRate.headTreeRate) {
						pos = this.getRandomPosition(i, j);
						this.objects[i][j] = [{
							id : ObjectType.headTreeId[Math.round(cc.random0To1()*(ObjectType.headTreeId.length - 2))],
							currentAge : 1,
							pos : pos,
					},];
					}
					
				}
				
				// 初始化人头萝卜（只生长在草地）
				if (this.grassArray[i][j] === mapType.grass && this.objects[i][j] === -1) {
					if (cc.random0To1() < mapTypeRate.headRadishRate) {
						pos = this.getRandomPosition(i, j);
						this.objects[i][j] = [{
							id : ObjectType.headRadishId[0],
							currentAge : 1,
							pos : pos,
					},];
					}
				}
				
				// 初始化人头草
				if (this.objects[i][j] === -1 && this.rockArray[i][j] === mapType.rock) {
					if (cc.random0To1() < mapTypeRate.headGrassRate) {
						pos = this.getRandomPosition(i, j);
						this.objects[i][j] = {
								id : ObjectType.headGrassId[Math.round(cc.random0To1()*(ObjectType.headGrassId.length - 2))],
								currentAge : 1,
								pos : pos,
						};
					}
				}
				
				// 初始化骷髅石
				if (this.objects[i][j] === -1  && this.rockArray[i][j] === mapType.rock && 
						this.grassArray[i][j] !== mapType.grass) {
					if (cc.random0To1() < mapTypeRate.headStoneRate) {
						pos = this.getRandomPosition(i, j);
						this.objects[i][j] = {
								id : ObjectType.headStoneId[Math.round(cc.random0To1())],
								currentAge : 1,
								pos : pos,
						};
					}
				}
			}
		}
	},
	
	// 初始化地图矩阵数据
	initMap : function () {

		var i , j;
		var startX, startY, endX, endY;

		startX = Math.ceil(this.viewWidth);
		startY = Math.ceil(this.viewHeight);
		endX = mapDesignSize.width - startX;
		endY = mapDesignSize.height - startY;

		// 初始化地图矩阵
		for (i = 0; i < mapDesignSize.width; i++) {
			var array1 = [];
			var array2 = [];
			var array3 = [];
			for(j = 0; j < mapDesignSize.height; j++){
				array1[j] = mapType.water;
				array2[j] = mapType.water;
				array3[j] = -1;
			}
			this.rockArray[i] = array1;
			this.grassArray[i] = array2;
			this.objects[i] = array3;
		}


		for (i = startX; i < endX; i++) {
			for (j = startY; j < endY; j++) {
				if (cc.random0To1() <= mapTypeRate.rockRate) {
					this.rockArray[i][j] = mapType.rock;
					if (cc.random0To1() <= mapTypeRate.grassRate) {
						this.grassArray[i][j] = mapType.grass;
					}
				} 
			}

		}
	},
	
	// 生成随机地图数据
	generateMapData : function(rate, type, anthorType, layerArray) {

		var i, j, sum;
		for (var n = 0; n < 5; n++) {
			var arrayCopy = layerArray;
			for (i = 1; i < mapDesignSize.width - 2; i++) {
				for (j = 1; j < mapDesignSize.height - 2; j++) {

					sum = arrayCopy[i-1][j-1] + arrayCopy[i][j-1] + arrayCopy[i+1][j-1]
					+ arrayCopy[i][j-1] + arrayCopy[i][j] + arrayCopy[i][j+1]
					+ arrayCopy[i-1][j+1] + arrayCopy[i][j+1] + arrayCopy[i+1][j+1];
					sum = sum / type;
					if (sum / 9 >= (1.5 - rate) * rate) {
						layerArray[i][j] = type;
					} else {
						layerArray[i][j] = anthorType;
					}

				}
			}
		}
	},
	
	// 随机生成主角坐标并初始化视角
    initPlayerPos : function () {
    	
    	var array = [], index;
    	for (var i in this.rockArray) 
    		for (var j in this.rockArray[i]) {
    			if (this.rockArray[i][j] === mapType.rock) {
    				
    				array.push({
					 	x : i,
					 	y : j,
					});
    			}
    		}
    	
    	index = Math.round(cc.random0To1() * (array.length - 1));
    	return array[index];
    },
	
	// 获取玩家初始化坐标
	getPlayerInitPos : function() {
		return this.playerPos;
	},
	
	// 获取绘制窗口大小
	getDrawRect : function() {
		return this.drawRect;
	},
 
	// 获取rock层矩阵
	getRockArray : function() {
		return this.rockArray;
	},
	
	// 获取grass层矩阵
	getGrassArray : function() {
		return this.grassArray;
	},
	
	// 获取视野宽度
	getViewWidth : function() {
		return this.viewWidth;
	},
	
	// 获取视野高
	getViewHeight : function() {
		return this.viewHeight;
	},
	
	// 设置物品矩阵信息
	setObjectArray : function(i, j, value) {
		this.objects[i][j] = value;
	},

	getPosAt : function(position) {
		return cc.p(Math.floor(position.x / tileDesignSize.width), Math.floor(position.y / tileDesignSize.height));
	},
};