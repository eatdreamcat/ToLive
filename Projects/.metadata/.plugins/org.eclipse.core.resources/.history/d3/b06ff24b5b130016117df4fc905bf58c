// 游戏世界场景
var GameScene = cc.Scene.extend({
	
	tiledMap : null, // 地图节点
	rockLayer : null, // 岩石层
	waterRightLayer : null, // 水层
	waterLeftLayer : null,
	grassLayer : null, // 草地层
	currentObject : {}, // 
	isTouchGoods : false,
	maxZOrder  : 0, 
	isTest : false,
	count : 0,
	isReset : false, 
	
	mainUILayer : null, // 主界面
	initLayer   : null, // 初始化界面
	moveStartPos : null, 
	
    direction : null, // 移动的方向
    moveSpeed : 0,
    
    // 地图相关
    
    winSize : null, // 窗口小大
    moveLimit : null, // 移动时坐标限制
	name : "GameScene",
	rockLayerArray : [], // 岩石层地图矩阵，用来生成地图
	grassLayerArray: [], // 草底层
	spritesArray   : {},
	
	drawRect   : null, // 绘制窗口的范围(地图坐标范围)
	isRestart : null,
	// 游戏时间
	time : null,
	second : 0,
	ctor : function (isRestart) {
		
		this._super();
		
		this.isRestart = isRestart;
		this.setName("GameScene");
	},
	
	onEnter : function() {
		this._super();
		
        this.prepare();
	},

	onEnterTransitionDidFinish : function() {
		this._super();
	
	},

	// 游戏前准备工作
	prepare : function() {
		
		this.addTiledMap();
//		this.initLayer = new InitLayer(this);
//		this.addChild(this.initLayer, WorldZOrder.InitLayerZOrder);
		
		Game.init(this);
		this.addMainUILayer();
		if (this.isRestart === false) {
			this.initLayer = UIManager.showUILayer(UILayer.InitLayer);
			this.initLayer.init(this);
		} else {
			this.initGame();
		}
	},
	
	// 初始化游戏
	initGame : function() {
		
		Game.initAllData();
		this.rockLayerArray = Game.getRockArray();
		this.grassLayerArray = Game.getGrassArray();
		this.drawRect = Game.getDrawRect();
		this.drawMap();
		this.initPlayer();
		UIManager.closeUILayer(UILayer.InitLayer);
		this.mainUILayer.getWidget().setTouchEnabled(true);
		this.mainUILayer.getWidget().setSwallowTouches(false);
		this.time = 0;
		this.second = 0;
	},
	
	// 游戏计时器
	gameUpdate : function(dt) {
		
		this.time++;
		// 到达植物生长周期
		if (this.time % TimeCycle.plantGrowCycle === 0) {

		}
		
        Player.setCurrentHungry(- Value.hungryReduceValue);
        
        var event = new cc.EventCustom(EventName.playerUpdate);
        cc.eventManager.dispatchEvent(event);
       
		// 更新时间显示
		this.mainUILayer.refreshClock(dt);
	},
	
	onExitTransitionDidFinish : function() {
		this._super();
		
	},

	onExit : function() {
		this._super();
		// 取消所有调度
		this.deleteAll();
	},
	
	deleteAll : function() {
		
		cc.log("释放资源");
		
		this.stopAllActions();
		this.unscheduleAllCallbacks();
		this.waterLeftLayer.clearAll();
		this.waterRightLayer.clearAll();
		this.rockLayer.clearAll();
		
		this.removeAllChildren();
	},
	
	// 添加主界面层
	addMainUILayer : function() {
	
		var mainUIWidget, drawButton, resetButton, grassButton;
//		this.mainUILayer = new MainUILayer();
//		this.addChild(this.mainUILayer, WorldZOrder.mainUIZOrder);
		this.mainUILayer = UIManager.showUILayer(UILayer.MainUILayer);
		
		// 添加触摸事件
	    mainUIWidget = this.mainUILayer.getWidget();
	    mainUIWidget.setTouchEnabled(false);
	    mainUIWidget.addTouchEventListener(this.onTouchEvent.bind(this), mainUIWidget);
	    
	    drawButton = mainUIWidget.getChildByNameRecursively("drawButton");
	    drawButton.addTouchEventListener(this.drawButtonTouch.bind(this), this.mainUILayer);
	    resetButton = mainUIWidget.getChildByNameRecursively("resetButton");
	    resetButton.addTouchEventListener(this.resetButtonTouch.bind(this), this.mainUILayer);
	    grassButton = mainUIWidget.getChildByNameRecursively("grassButton");
	    grassButton.addTouchEventListener(this.grassButtonTouch.bind(this), this.mainUILayer);
	},
	
	// 添加地图
	addTiledMap : function () {
		
	
	    this.tiledMap = new TLMap();
	    
	    this.waterRightLayer = createTLMapLayer(mapDesignSize, tileDesignSize, res.WaterRight, LayerName.waterRight, mapType.water);
	    this.waterLeftLayer = createTLMapLayer(mapDesignSize, tileDesignSize, res.WaterLeft, LayerName.waterLeft, mapType.water);
	    this.rockLayer = createTLMapLayer(mapDesignSize, tileDesignSize, res.Rock, LayerName.rock, mapType.rock);
	    this.grassLayer = createTLMapLayer(mapDesignSize, tileDesignSize, res.Grass, LayerName.grass, mapType.grass);
	    
	    this.winSize = cc.director.getWinSize();
	    this.maxZOrder = mapDesignSize.height * tileDesignSize.height;
	    // 角色移动时最大移动的范围
	    this.moveLimit = {
	        right : -mapDesignSize.width * tileDesignSize.width + this.winSize.width,
	        bottom : -mapDesignSize.height * tileDesignSize.height + this.winSize.height,
	        left : -tileDesignSize.width,
	        top : -tileDesignSize.height,
	    }
	    
	    this.tiledMap.setLayer(this.waterLeftLayer, mapType.water);
	    this.tiledMap.setLayer(this.waterRightLayer, mapType.water);
	    this.tiledMap.setLayer(this.rockLayer, mapType.rock);
	    this.tiledMap.setLayer(this.grassLayer, mapType.grass);
	    this.tiledMap.setPosition(0, 0);
	    this.addChild(this.tiledMap, WorldZOrder.TiledMapZOder);
	    
	    // 初始化水层运动
	    var waterActionRight, waterActionLeft, moveBy;
	    moveBy = cc.moveBy(1.5, cc.p(tileDesignSize.width, 0));
	    waterActionRight = cc.sequence([moveBy, moveBy.reverse()]);
	    moveBy = cc.moveBy(1.5, cc.p(-tileDesignSize.width, 0));
	    waterActionLeft = cc.sequence([moveBy, moveBy.reverse()]);
	    this.waterRightLayer.runAction(waterActionRight.repeatForever());
	    this.waterLeftLayer.runAction(waterActionLeft.repeatForever());
	},
	
	// 创建角色
	initPlayer : function() {
		
		var playerPos, mapPos, viewWidth, viewHeight;
		playerPos = Game.getPlayerInitPos();
		viewWidth = Game.getViewWidth();
		viewHeight = Game.getViewHeight();
		Player = new PlayerNode();
		this.moveSpeed = Player.getSpeed();
		Player.setPosition(this.waterRightLayer.getPositionAt(playerPos));
		this.tiledMap.addChild(Player, 
				WorldZOrder.playerZOrder + this.maxZOrder - Math.round(Player.getPosition().y));
		
		mapPos = this.waterRightLayer.getPositionAt(playerPos);
		this.tiledMap.setPosition(-mapPos.x + this.winSize.width / 2, -mapPos.y + this.winSize.height / 2);	
		this.scheduleUpdate();
		
		// 派发事件
		var event = new cc.EventCustom(EventName.addGoodsEvent);
		cc.eventManager.dispatchEvent(event);
	},
	
	// 触摸事件
	onTouchEvent : function (sender, type) {
		
		var touchStartPos, touchEndedPos, touchMovingPos;
		switch (type) {
		case ccui.Widget.TOUCH_BEGAN:
			
			touchStartPos = sender.getTouchBeganPosition();
			break;
		case ccui.Widget.TOUCH_MOVED:
			touchMovingPos = sender.getTouchMovePosition();
			/*
			 * 计算方向
			 */
			if (this.moveStartPos == null) {
				this.moveStartPos = touchMovingPos;
			} else if (cc.pDistance(touchMovingPos, this.moveStartPos) > TouchMoveLimit) {
				
				this.direction = cc.p(Math.round(touchMovingPos.x - this.moveStartPos.x), 
						Math.round(touchMovingPos.y - this.moveStartPos.y));
				// 设置角色为移动状态
				Player.setCurrentState(PlayerState.Move);
				this.moveStartPos = touchMovingPos;
			}
			break;
		case ccui.Widget.TOUCH_ENDED:
			
			touchEndedPos = sender.getTouchEndPosition();
			this.moveStartPos = null;
			
            // 设置角色为空闲状态
            Player.setCurrentState(PlayerState.Idle);
			break;
		default:
		    // 设置角色为空闲状态
		    Player.setCurrentState(PlayerState.Idle);
		    this.moveStartPos = null;
			break;
		}
	},
	
	// 朝某个方向移动(用来控制视角移动)
	moveMapBy : function () {
		
		var x, y;
		var n, offset;
		if (this.direction.y === 0 && this.direction.x !== 0) {
			y = 0;
			x = this.moveSpeed * (this.direction.x / Math.sqrt(this.direction.x * this.direction.x));
		} else if (this.direction.x === 0 && this.direction.y !== 0) {
			x = 0;
			y = this.moveSpeed * (this.direction.y / Math.sqrt(this.direction.y * this.direction.y));
		} else if (this.direction.x !== 0 && this.direction.y !== 0){
			n = this.direction.x / this.direction.y;
			y = this.moveSpeed / (Math.sqrt(n * n + 1)) * this.direction.y / Math.sqrt(this.direction.y * this.direction.y);
			x = n * y;
		} else {
			x = 0;
			y = 0;
		}
		
		offset = this.checkMapBorder(x, y);
		if (offset.x === 0 && offset.y === 0) {
			return;
		}
		this.tiledMap.x += offset.x;
		this.tiledMap.y += offset.y;
		Player.x -= offset.x;
		Player.y -= offset.y;
		this.updateDrawRect(cc.p(Player.x + offset.x, Player.y + offset.y), cc.p(Player.x, Player.y));
		
	},
	
	// 刷新函数
    update : function (dt) {
    	
		if (Player.getCurrentState() === PlayerState.Move) {
			Player.setLocalZOrder(WorldZOrder.playerZOrder + this.maxZOrder - Math.round(Player.getPosition().y));
			this.moveMapBy();
		} 
		
		this.second += dt;
		if (this.second >= 1) {
			this.gameUpdate(this.second);
			this.scriptsUpdate(this.second);
			this.second = 0;
		}
	},
	
	// 监测地图移动边界限制
	checkMapBorder : function (x, y) {
		
		var mapPosition;
		var playerPos, tile;
		var posX = x, posY = y;
		mapPosition = this.tiledMap.getPosition();
		playerPos = Player.getPosition();
		
		// 检测横向范围
		if (mapPosition.x + x > this.moveLimit.left && x > 0){
			posX = 0;
		} else if (mapPosition.x + x < this.moveLimit.right && x < 0) {
			posX = this.moveLimit.right - mapPosition.x;
		}
		
		// 检测纵向范围
		if (mapPosition.y + y > this.moveLimit.top && y > 0){
			posY = 0;
		} else if (mapPosition.y + y < this.moveLimit.bottom && y < 0) {
			posY = this.moveLimit.bottom - mapPosition.y;
		}
		
		// 检测角色是否可以走水路
		if (Player.getCurrentState() !== PlayerState.InBoat) {
			if (this.rockLayer.getTileAt(this.getPosInTile(cc.p(playerPos.x - posX, playerPos.y - posY))) == null) {
				posX = 0;
				posY = 0;
			} else if (this.rockLayer.getTileAt(this.getPosInTile(cc.p(playerPos.x, playerPos.y - posY))) == null) {
				posY = 0;
			} else if (this.rockLayer.getTileAt(this.getPosInTile(cc.p(playerPos.x - posX, playerPos.y))) == null) {
				posX = 0;
			}
		}
		return cc.p(posX, posY);
	},
	
	// 获取瓦片地图中的坐标
	getPosInTile : function (position) {
		return cc.p(parseInt(position.x / tileDesignSize.width), 
				parseInt(position.y / tileDesignSize.height));
	},
	
	// 根据视角变化更新地图
	updateMap : function(newArray, oldArray) {
		
		this.waterLeftLayer.updateMap(newArray, oldArray);
		this.waterRightLayer.updateMap(newArray, oldArray);
		this.rockLayer.updateMap(newArray, oldArray);
		this.grassLayer.updateMap(newArray, oldArray);
		
		this.drawWorldSprite();
	},
	
	// 根据地图矩阵信息绘制地图
	drawMap : function() {
		
		this.waterLeftLayer.drawMapWithRange(this.drawRect);
		this.waterRightLayer.drawMapWithRange(this.drawRect);
		this.rockLayer.drawMapWithRangeAndArray(this.drawRect, this.rockLayerArray);
		this.grassLayer.drawMapWithRangeAndArray(this.drawRect, this.grassLayerArray);
	    
		// 绘制精灵
		this.drawWorldSprite();
	},

	// 绘制环境精灵
	drawWorldSprite : function() {
		
		var sprite, i, j, id, livingObject, pos, tag;
		var objectArray = Game.getObjectArray();
		for (i in this.spritesArray) {
			this.tiledMap.removeChild(this.spritesArray[i], true);
			delete this.spritesArray[i];
		}
		this.spritesArray = {};
		
		for (i = this.drawRect.startX; i < this.drawRect.endX + 1; i++) {
			for (j = this.drawRect.startY; j < this.drawRect.endY + 1; j++) {
				if (objectArray[i] == null) {
					break;
				}
				
				if (objectArray[i][j] != null) {
					
					for (var z in objectArray[i][j]) {
						id = objectArray[i][j][z].id;
						if (id !== -1 && id != null) {
							livingObject = LivingConfig.getItemById(id);

							if (livingObject != null) {
								sprite = new ccui.ImageView();
								tag = parseInt(z) + (i + j  * mapDesignSize.width)*maxNumForTile;
								sprite.setTag(tag);
								sprite.setTouchEnabled(true);
								sprite.setScale(resScale);
								sprite.loadTexture(livingObject.pic, ccui.Widget.PLIST_TEXTURE);
								// pos = this.waterRightLayer.getPositionAt(cc.p(i, j));
								sprite.setAnchorPoint(0.5, 0);
								//sprite.setPosition(pos.x + tileDesignSize.width / 2, pos.y);
								pos = objectArray[i][j][z].pos;
								sprite.setPosition(pos);
								sprite.setUserData({
									id : id,
									z : parseInt(z),
								});
								this.tiledMap.addChild(sprite, WorldZOrder.playerZOrder + this.maxZOrder - pos.y);
								this.spritesArray[tag] = sprite;

								// 添加触摸事件
								sprite.addTouchEventListener(this.onGoodsTouch.bind(this), this);
							}
						}
					}
				}
			}
		}
	},
	
	// 绘制单个精灵
	drawSingleSprite : function(i, j) {
	
		var sprite, tag, gameObject, id, pic;
		gameObject = Game.getObjectArray();
		if (gameObject[i][j] != null) {
			for (var z in gameObject[i][j]) {
				id = gameObject[i][j][z].id;
				if (id != null && id != -1 && id != "null") {
					tag = parseInt(z) + (i + j  * mapDesignSize.width)*maxNumForTile;
					if (this.spritesArray[tag] == null) {
						sprite = new ccui.ImageView();
						sprite.setTag(tag);
						sprite.setTouchEnabled(true);
						sprite.setScale(resScale);
						pic = LivingConfig.getItemById(id).pic;
						sprite.loadTexture(pic, ccui.Widget.PLIST_TEXTURE);
						sprite.setAnchorPoint(0.5, 0);
						pos = gameObject[i][j][z].pos;
						sprite.setPosition(pos);
						sprite.setUserData({
							id : id,
							z : parseInt(z),
						});
						this.tiledMap.addChild(sprite, WorldZOrder.playerZOrder + this.maxZOrder - pos.y);
						this.spritesArray[tag] = sprite;

						// 添加触摸事件
						sprite.addTouchEventListener(this.onGoodsTouch.bind(this), this);
					}
				}
			}
		}
	},
	
	// 处理长按操作
	scriptsUpdate : function(dt) {
		
	},
	
	// 获取当前目标对象
	getCurrentObject : function() {
	    return this.currentObject;	
	},
	
	// 精灵点击事件
	onGoodsTouch : function(sender, type) {
		
		var id, tag, health;
		var userData, pos;
		var applyScript;
		if (type === ccui.Widget.TOUCH_BEGAN) {
			
			// 高亮显示
			//sender.setHighLightEnable(true);
			
			userData = sender.getUserData();
			id = userData.id;
			tag = sender.getTag();
			pos = sender.getPosition();
			health = LivingConfig.getItemById(id).health;
			
			if (this.currentObject.id != id) {
				this.currentObject = {
						id  : id,
						tag : tag,
						health : health, 
						};
			} 
			
			if (this.canReach(pos)) {
				applyScript = LivingConfig.getItemById(id).applyScript;
				if (applyScript != null && applyScript != "null") {
					cc.log("执行脚本：" + applyScript);
					var s = applyScript + "(id, sender, this)";
					eval(s);
				} else {
					//this.mainUILayer.setLogLabel(applyScript);
				}
			} else {
				this.mainUILayer.setLogLabel("距离太远了");
			}
			
		} else if (type === ccui.Widget.TOUCH_ENDED ||
				type === ccui.Widget.TOUCH_CANCELED) {
			//sender.setHighLightEnable(false);
		}
	},
	
	// 检测任务与点击物品的距离
	canReach : function (pos) {
		var playerPos, distance;
		playerPos = Player.getPosition();
		distance = cc.pDistance(pos, playerPos);
		if (distance <= reachRange) {
			return true;
		}
		return false;
	},
	
//	// 更新视角框
//	updateDrawRect : function(lastPos, currentPos) {
//		
//		var lastP, currentP, offsetX, offsetY;
//		var newArray = [], oldArray = [];
//		lastP = this.waterRightLayer.getPosAt(lastPos);
//		currentP = this.waterRightLayer.getPosAt(currentPos);
//		offsetX = currentP.x - lastP.x;
//		offsetY = currentP.y - lastP.y;
////		cc.log("offsetX = " + offsetX + " offsetY = " + offsetY);
//		if (!this.isTest) {
//			
//			if (offsetX > 0) {
//				if (offsetY > 0) {
//					/*
//					 * 记录移除视野外的pos集
//					 */
//					for (var i = this.drawRect.startX; i < this.drawRect.startX + offsetX; i++) {
//						for (var j = this.drawRect.startY; j < this.drawRect.endY; j++) {
//							oldArray.push(cc.p(i, j));
//						}
//					}
//					for (var i = this.drawRect.startX + offsetX; i < this.drawRect.endX; i++) {
//						for (var j = this.drawRect.startY; j < this.drawRect.startY + offsetY; j++) {
//							oldArray.push(cc.p(i, j));
//						}
//					}
//					/*
//					 * 记录新移入视野的pos集
//					 */
//					 for (var i = this.drawRect.startX + offsetX; i < this.drawRect.endX + offsetX; i++) {
//						 for (var j = this.drawRect.endY; j < this.drawRect.endY + offsetY; j++) {
//							 newArray.push(cc.p(i, j));
//						 }
//					 }
//					 for (var i = this.drawRect.endX; i < this.drawRect.endX + offsetX; i++) {
//						 for (var j = this.drawRect.startY + offsetY; j < this.drawRect.endY; j++) {
//							 newArray.push(cc.p(i, j));
//						 }
//					 }
//				} else if (offsetY < 0) {
//					/*
//					 * 记录移除视野外的pos集
//					 */
//					for (var i = this.drawRect.startX; i < this.drawRect.endX; i++) {
//						for (var j = this.drawRect.endY + offsetY; j < this.drawRect.endY; j++) {
//							oldArray.push(cc.p(i, j));
//						}
//					}
//
//					for (var i = this.drawRect.startX; i < this.drawRect.startX + offsetX; i++) {
//						for (var j = this.drawRect.startY; j < this.drawRect.endY + offsetY; j++) {
//							oldArray.push(cc.p(i, j));
//						}
//					}
//
//					/*
//					 * 记录新移入视野的pos集
//					 */
//					 for (var i = this.drawRect.startX + offsetX; i < this.drawRect.endX + offsetX; i++) {
//						 for (var j = this.drawRect.startY + offsetY; j < this.drawRect.startY; j++) {
//							 newArray.push(cc.p(i, j));
//						 }
//					 }
//
//					 for (var i = this.drawRect.endX; i < this.drawRect.endX + offsetX; i++) {
//						 for (var j = this.drawRect.startY + offsetY; j < this.drawRect.endY + offsetY; j++) {
//							 newArray.push(cc.p(i, j));
//						 }
//					 }
//				} else {
//					for (var i = this.drawRect.startX; i < this.drawRect.startX + offsetX; i++) {
//						for(var j = this.drawRect.startY; j < this.drawRect.endY; j++){
//							oldArray.push(cc.p(i, j));
//						}
//					}
//					for (var i = this.drawRect.endX; i < this.drawRect.endX + offsetX; i++) {
//						for (var j = this.drawRect.startY; j < this.drawRect.endY; j++) {
//							newArray.push(cc.p(i, j));
//						}
//					}
//				}
//			} else if (offsetX < 0) {
//				if (offsetY > 0) {
//					/*
//					 * 记录移除视野外的pos集
//					 */
//					for (var i = this.drawRect.startX; i < this.drawRect.endX; i++) {
//						for (var j = this.drawRect.startY; j < this.drawRect.startY + offsetY; j++) {
//							oldArray.push(cc.p(i, j));
//						}
//					}
//					for (var i = this.drawRect.endX + offsetX; i < this.drawRect.endX; i++) {
//						for (var j = this.drawRect.startY + offsetY; j < this.drawRect.endY; j++) {
//							oldArray.push(cc.p(i, j));
//						}
//					}
//					/*
//					 * 记录新移入视野的pos集
//					 */
//					for (var i = this.drawRect.startX + offsetX; i < this.drawRect.startX; i++) {
//						for (var j = this.drawRect.startY + offsetY; j < this.drawRect.endY + offsetY; j++) {
//							newArray.push(cc.p(i, j));
//						}
//					}
//					for (var i = this.drawRect.startX; i < this.drawRect.endX + offsetX; i++) {
//						for (var j = this.drawRect.endY; j < this.drawRect.endY + offsetY; j++) {
//							newArray.push(cc.p(i, j));
//						}
//					}
//				} else if (offsetY < 0) {
//					/*
//					 * 记录移除视野外的pos集
//					 */
//					for (var i = this.drawRect.startX; i < this.drawRect.endX; i++) {
//						for (var j = this.drawRect.endY + offsetY; j < this.drawRect.endY; j++) {
//							oldArray.push(cc.p(i, j));
//						}
//					}
//					for (var i = this.drawRect.endX + offsetX; i < this.drawRect.endX; i++) {
//						for (var j = this.drawRect.startY; j < this.drawRect.endY + offsetY; j++) {
//							oldArray.push(cc.p(i, j));
//						}
//					}
//					/*
//					 * 记录新移入视野的pos集
//					 */
//					for (var i = this.drawRect.startX + offsetX; i < this.drawRect.startX; i++) {
//						for (var j = this.drawRect.startY + offsetY; j < this.drawRect.endY + offsetY; j++) {
//							newArray.push(cc.p(i, j));
//						}
//					}
//					for (var i = this.drawRect.startX; i < this.drawRect.endX + offsetX; i++) {
//						for (var j = this.drawRect.startY + offsetY; j < this.drawRect.startY; j++) {
//							newArray.push(cc.p(i, j));
//						}
//					}
//				} else {
//					for (var i = this.drawRect.endX + offsetX; i < this.drawRect.endX; i++) {
//						for (var j = this.drawRect.startY; j < this.drawRect.endY; j++) {
//							oldArray.push(cc.p(i, j));
//						}
//					}
//					
//					for (var i = this.drawRect.startX + offsetX; i < this.drawRect.startX; i++) {
//						for (var j = this.drawRect.startY; j < this.drawRect.endY; j++) {
//							newArray.push(cc.p(i, j));
//						}
//					}
//				}
//			} else {
//				if (offsetY > 0) {
//                    for (var i = this.drawRect.startX; i < this.drawRect.endX; i++) {
//                    	for (var j = this.drawRect.startY; j < this.drawRect.startY + offsetY; j++) {
//                    		oldArray.push(cc.p(i, j));
//                    	}
//                    }
//                    
//                    for (var i = this.drawRect.startX; i < this.drawRect.endX; i++) {
//                    	for (var j = this.drawRect.endY; j < this.drawRect.endY + offsetY; j++) {
//                    		newArray.push(cc.p(i, j));
//                    	}
//                    }
//				} else if (offsetY < 0) {
//					for (var i = this.drawRect.startX; i < this.drawRect.endX; i++) {
//						for (var j = this.drawRect.endY + offsetY; j < this.drawRect.endY; j++) {
//							oldArray.push(cc.p(i, j));
//						}
//					}
//
//					for (var i = this.drawRect.startX; i < this.drawRect.endX; i++) {
//						for (var j = this.drawRect.startY + offsetY; j < this.drawRect.startY; j++) {
//							newArray.push(cc.p(i, j));
//						}
//					}
//				} else {
//                    newArray = [];
//                    oldArray = [];
//				}
//			}
//			this.drawRect.startX += offsetX;
//			this.drawRect.endX += offsetX;
//			this.drawRect.startY += offsetY;
//			this.drawRect.endY += offsetY;
//			//this.updateMap(newArray, oldArray);
//			this.drawMap();
//		}
//	},
	
	// 更新视角框
	updateDrawRect : function(lastPos, currentPos) {

		var lastP, currentP;
		lastP = this.waterRightLayer.getPosAt(lastPos);
		currentP = this.waterRightLayer.getPosAt(currentPos);
		if (!this.isTest) {
			if (lastP.x !== currentP.x || lastP.y !== currentP.y) {
				this.drawRect.startX += (currentP.x - lastP.x);
				this.drawRect.endX += (currentP.x - lastP.x);
				this.drawRect.startY += (currentP.y - lastP.y);
				this.drawRect.endY += (currentP.y - lastP.y);
				this.drawMap();
			}
		}
	},
	
 	//测试绘图
 	testDraw : function() {
	
 		this.drawRect = {
 				startX : 0,
 				endX : mapDesignSize.width,
 				startY : 0,
 				endY : mapDesignSize.height,
 		}
 		this.drawMap();
 		this.tiledMap.setPosition(0, 0);
 		this.tiledMap.setScaleX(this.winSize.width / (mapDesignSize.width * tileDesignSize.width));
 		this.tiledMap.setScaleY(this.winSize.height / (mapDesignSize.height * tileDesignSize.height));
	},
	
 	// 重置地图按钮
 	resetButtonTouch : function(sender, type) {

 		if (type === ccui.Widget.TOUCH_ENDED) {
 			this.count = 0;
 			this.isReset = true;
 			this.mainUILayer.setLogLabel("");
 			// 初始化地图数据
 			Game.initMap();
 			this.testDraw();		
 		}
 	},
 	
 	// 生成地图按钮按下
 	drawButtonTouch : function(sender, type) {

 		if (type === ccui.Widget.TOUCH_ENDED) {
 			this.isTest = true;
 			this.count++;
 			this.mainUILayer.setLogLabel("第"+this.count+"次");
 			if (this.isReset === true) {
 				Game.generateMapData(mapTypeRate.rockRate, mapType.rock, mapType.water, this.rockLayerArray);
 				Game.generateMapData(mapTypeRate.rockRate, mapType.grass, mapType.water, this.grassLayerArray);
 				Game.initObjectData();
 			}
 			this.testDraw();
 		}
 	},
 	
 	// 隐藏草地层
 	grassButtonTouch : function(sender, type) {
		
 		if (type === ccui.Widget.TOUCH_ENDED) {
 			this.grassLayer.setVisible(!this.grassLayer.isVisible());
 		}
	},

 	// 获取草地层矩阵数据
 	getGrassLayerArray : function () {
		return this.grassLayerArray;
	},
	
	// 获取岩石层矩阵数据
	getRockLayerArray : function () {
		return this.rockLayerArray;
	},
	
	// 获取当前视角范围
	getCurrentViewRange : function() {
		return this.drawRect;
	},
	
	// 获取主界面
	getMainUILayer : function () {
		return this.mainUILayer;
	},
	
	// 获取地图
	getTiledMap : function() {
		return this.tiledMap;
	},
});