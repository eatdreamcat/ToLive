/*created by wangchichi
 * 2016.03.11
 * 拓展一些引擎的方法
 */

// 根据name递归获取子节点
cc.Node.prototype.getChildByNameRecursively = function (name) {
	
	var child = this.getChildByName(name);
	if (child != null) {
		return child;
	} else {
		var children = this.getChildren();
		var result;
		for (var i in children) {
			result = children[i].getChildByNameRecursively(name);
			if (result != null) {
				return result;
			}
		}
	}
	return null;
};

// 高亮
ccui.Widget.prototype.setHighLightEnable = function (isHighLight) {
	
	var gl;
	gl = this.getShaderProgram();
	if (gl == null) {
		gl = new cc.GLProgram();
	}
	if (isHighLight) {
		gl.init(res.highLightVsh, res.highLightFsh);
	} else {
		
	}
	gl.link();
	gl.updateUniforms();
	this.setShaderProgram(gl);
	gl.release();
};