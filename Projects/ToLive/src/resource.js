var res = {

    // UI界面
    LoadingLayer : "res/UI/LoadingLayer.ExportJson",
    MainUILayer  : "res/UI/MainUILayer.ExportJson",
    InitLayer    : "res/UI/InitLayer.ExportJson",
    GameOverLayer : "res/UI/GameOverLayer.ExportJson",
    CompositeLayer : "res/UI/CompositeLayer.ExportJson",
    
    // 地图
    WaterRight : "res/map/waterRight.png",
    WaterLeft  : "res/map/waterLeft.png",
    Wet        : "res/map/wet.png",
    Rock       : "res/map/rock.png",
    Grass      : "res/map/grass.png",
    GamePng      : "res/png/game.png",
    GamePlist    : "res/png/game.plist",
   
    
    // 动画
    playerPng    : "res/animation/Player/Player0.png",
    playerPlist  : "res/animation/Player/Player0.plist",
    playerAnimation : "res/animation/Player/Player.ExportJson",
    
    // 配置文件
    livingConfig : "res/config/living.json",
    toolTypeConfig : "res/config/toolType.json",
    toolsConfig  : "res/config/tools.json",
    
    // shader着色器文件
    highLightVsh : "res/shader/highLight.vsh",
    highLightFsh : "res/shader/highLight.fsh",
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
};