/**
 * livingConfig
 */
var LivingConfig = {
		
		items : null, // 配置表中的所有item 
		// 初始化
		init : function () {
			
			var self = this;
			cc.loader.loadJson(res.livingConfig, function (err) {
				if (err) {
					cc.log("LiveingConfig Load Failed :" + err);
				} else {
					self.items = cc.loader.getRes(res.livingConfig);
					for (var i in self.items) {
						if (self.items[i].effect != null && self.items[i].effect != "null") {
							if (typeof self.items[i].effect === "string") {
								self.items[i].effect = JSON.parse(self.items[i].effect);
							}
						}
					}
				}
			});
			
		},
		
		// 获取所有item
		getAllItems : function() {
			return this.items;
		},
		
		// 根据id获取item
		getItemById : function(id) {
			if (this.items == null) {
				return null;
			}
			return this.items[id];
		},
		
		
}