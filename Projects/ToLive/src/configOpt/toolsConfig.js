/**
 * toolsConfig
 */
var ToolsConfig = {

		items : null, // 配置表中的所有item 
		// 初始化
		init : function () {

			var self = this;
			cc.loader.loadJson(res.toolsConfig, function (err) {
				if (err) {
					cc.log("ToolsConfig Load Failed :" + err);
				} else {
					self.items = cc.loader.getRes(res.toolsConfig);
					for (var i in self.items) {
						if (self.items[i].material != null && self.items[i].material != "null") {
							if (typeof self.items[i].material === "string") {
								self.items[i].material = JSON.parse(self.items[i].material);
							}							
						}
					}
					
				}
			});

		},

		// 获取所有item
		getAllItems : function() {
			return this.items;
		},

		// 根据id获取item
		getItemById : function(id) {
			if (this.items == null) {
				return null;
			}
			return this.items[id];
		},
        
		// 根据工具类型获取工具集合
		getItemsByType : function (type) {
			
			var itemsByType = [];
			for (var i in this.items) {
				if (this.items[i].type === type) {
					itemsByType.push(this.items[i]);
				}
			}
			
			return itemsByType;
		},

}