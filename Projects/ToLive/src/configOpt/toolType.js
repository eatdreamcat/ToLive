/**
 * toolType.config
 */
var ToolTypeConfig = {

		items : null, // 配置表中的所有item 
		// 初始化
		init : function () {

			var self = this;
			cc.loader.loadJson(res.toolTypeConfig, function (err) {
				if (err) {
					cc.log("ToolTypeConfig Load Failed :" + err);
				} else {
					self.items = cc.loader.getRes(res.toolTypeConfig);
				}
			});
		},

		// 获取所有item
		getAllItems : function() {
			return this.items;
		},

		// 根据id获取item
		getItemById : function(id) {
			if (this.items == null) {
				return null;
			}
			return this.items[id];
		},


}