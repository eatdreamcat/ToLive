//游戏结束场景
var GameOverScene = cc.Scene.extend({

	ctor : function () {

		
		this._super();
		this.setName("GameOverScene");
	
		//this.addChild(new GameOverLayer());
	},

	onEnter : function() {
		this._super();
	
		UIManager.showUILayer(UILayer.GameOverLayer);
	},

	onEnterTransitionDidFinish : function() {
		this._super();
		
	},

	onExitTransitionDidFinish : function() {
		this._super();
		
	},

	onExit : function() {
		this._super();
		
	},
});