//加载界面
var LoadingScene = cc.Scene.extend({
	
	ctor : function () {
		
		this._super();
		this.setName("LoadingScene");
		
//		this.addChild(new LoadingLayer());
	},
	
	onEnter : function() {
	    this._super();
	  
	    UIManager.showUILayer(UILayer.LoadingLayer);
	},
	
	onEnterTransitionDidFinish : function() {
		this._super();
		
	},

	onExitTransitionDidFinish : function() {
	    this._super();
	   
	},
	
	onExit : function() {
		this._super();
		
	},
});
