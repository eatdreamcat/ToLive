//初始化世界界面
var InitLayer = cc.Layer.extend({
	
	gameScene : null,
	loadingBg : null,
	startButton : null, 
	exitButton  : null,
	
	ctor : function () {
	
		var widget;
		this._super();
		widget = ccs.uiReader.widgetFromJsonFile(res.InitLayer);
		this.loadingBg = widget.getChildByName("LoadingBg");
		this.loadingBg.setVisible(false);
		
		// 添加按钮
		this.startButton = widget.getChildByNameRecursively("StartButton");
		this.exitButton  = widget.getChildByNameRecursively("ExitButton");
		this.startButton.addTouchEventListener(this.startButtonTouch.bind(this), this);
		this.exitButton.addTouchEventListener(this.exitButtonTouch.bind(this), this);
		this.addChild(widget);
	},
	
	init : function(gameScene) {
		this.gameScene = gameScene;
	},
	
	// 开始按钮按下
	startButtonTouch : function(sender, type) {
		
		if (type === ccui.Widget.TOUCH_BEGAN) {
			this.setLoadingBg(true);
			this.gameScene.initGame();
		}
	},
	
	// 退出按钮按下
	exitButtonTouch : function(sender, type) {
		
		if (type === ccui.Widget.TOUCH_BEGAN) {
            cc.director.end();
		}
	},
	
	// 设置是否显示加载图片
	setLoadingBg : function(isShow) {
		this.loadingBg.setVisible(isShow);
	},
	
	onEnter : function() {
		this._super();
		this.loadingBg.setVisible(false);
	},

	onEnterTransitionDidFinish : function() {
		this._super();
	
	},

	onExitTransitionDidFinish : function() {
		this._super();
		
	},

	onExit : function() {
		this._super();
		
	},
	
});