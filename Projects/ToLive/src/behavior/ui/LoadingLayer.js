//加载界面
var LoadingLayer = cc.Layer.extend({
	
	changeSceneDelay : 2.5, // 切换场景时等待的时间（秒）
	action : null,
	ctor : function () {
		
		var widget = null;
		
		this._super();
		widget = ccs.uiReader.widgetFromJsonFile("res/UI/LoadingLayer.ExportJson");
		this.action = ccs.actionManager.getActionByName("res/UI/LoadingLayer.ExportJson", "Animation0");
		
		this.addChild(widget);
	},
	
	onEnter : function() {
		this._super();
		
		this.action.play();
		this.loadResource();
	},

	onEnterTransitionDidFinish : function() {
		this._super();
	},

	onExitTransitionDidFinish : function() {
		this._super();
		
		// 取消所有调度
		this.stopAllActions();
		this.unscheduleAllCallbacks();
	
	},

	onExit : function() {
		this._super();
		
	},
	
	// 加载各项资源
	loadResource : function () {
		
		// 加载js文件
		var self = this;
		cc.loader.loadJs(jsList, function (err) {
		    if (err) {
		        cc.log("Load Failed :" + err);
		    } else {
		    	
		    	// 加载资源
		    	cc.loader.load(g_resources, function (err) {
		    		if (err) {
		    			cc.log("Load Failed :" + err);
		    		} else {
		    			self.scheduleOnce(self.goToGameScene, self.changeSceneDelay);
		    		}
		    	});
		    }
		});
	},
	
	// 切换场景
	goToGameScene : function () {
		
		// 加载图片资源
		cc.spriteFrameCache.addSpriteFrames(res.GamePlist, res.GamePng);

		// 加载各种config文件
		LivingConfig.init();
		ToolTypeConfig.init();
		ToolsConfig.init();
		
		var gameScene =  new GameScene(false);
		cc.director.replaceScene(gameScene);
		//cc.director.popScene();
	},
	
});