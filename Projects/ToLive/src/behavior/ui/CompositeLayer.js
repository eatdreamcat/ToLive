//游戏结束界面
var CompositeLayer = cc.Layer.extend({

    icon1 : null,
    icon2 : null,
    bg1 : null,
    bg2 : null,
    numberLabel1 : null,
    numberLabel2 : null,
    contentLabel : null,
    compositeButton : null,
    icons : [],
    bgs : [],
    numberLabel : [],
    canComposite : 0,
    selectedItem : null,
	ctor : function () {

		var widget, closeButton, cancelButton;
		this._super();
		widget = ccs.uiReader.widgetFromJsonFile(res.CompositeLayer);
		
		// 关闭按钮
		closeButton = widget.getChildByNameRecursively("CloseButton");
		closeButton.addTouchEventListener(this.closeButton.bind(this), this);
		
	    // 合成按钮
		this.compositeButton = widget.getChildByNameRecursively("CompositeButton");
		this.compositeButton.addTouchEventListener(this.composite.bind(this), this);
		
		// 取消按钮
		cancelButton = widget.getChildByNameRecursively("CancelButton");
		cancelButton.addTouchEventListener(this.closeButton.bind(this), this);
		
		this.icon1 = widget.getChildByNameRecursively("icon1");
		this.icon2 = widget.getChildByNameRecursively("icon2");
		this.icons = [this.icon1, this.icon2];
		
		this.bg1 = widget.getChildByNameRecursively("bg1");
		this.bg2 = widget.getChildByNameRecursively("bg2");
		this.bgs = [this.bg1, this.bg2];
		
		this.numberLabel1 = widget.getChildByNameRecursively("NumberLabel1");
		this.numberLabel2 = widget.getChildByNameRecursively("NumberLabel2");
		this.numberLabel = [this.numberLabel1, this.numberLabel2];
		
		this.contentLabel = widget.getChildByNameRecursively("ContentLabel");
		
		this.addChild(widget);
	},

	// 刷新界面
	refreshUI : function (item) {
		
		var needNum, currentNum, living, index = 0;
		this.selectedItem = item;
		this.canComposite = 0;
		this.contentLabel.setString(item.content);
		for (var i in item.material) {
			living = LivingConfig.getItemById(i);
			needNum = item.material[i];
			currentNum = Player.getToolById(i).number;
			if (needNum > currentNum) {
				this.numberLabel[index].setColor(cc.color(255, 0, 0));
				this.compositeButton.setTouchEnabled(false);
			} else {
				this.numberLabel[index].setColor(cc.color(0, 255, 0));
				this.compositeButton.setTouchEnabled(true);
				this.canComposite++;
			}
			this.numberLabel[index].setString(currentNum + "/" + needNum);
			this.icons[index].loadTexture(living.pic, ccui.Widget.PLIST_TEXTURE);
			index ++;
		}
		
	},
	
	// 合成
	composite : function(sender, type) {
		
		var currentScene = cc.director.getRunningScene();
		if (type === ccui.Widget.TOUCH_BEGAN) {
			if (this.canComposite > 1) {
				for (var i in this.selectedItem.material) {
					Player.removeToolById(i, this.selectedItem.material[i]);
				}
				var applyScript = LivingConfig.getItemById(this.selectedItem.productId).applyScript;
				
				if (applyScript != null && applyScript != "null") {
					var s = applyScript + "(this.selectedItem.productId, null, currentScene)";
					eval(s);
					UIManager.closeUILayer(UILayer.CompositeLayer);
				}
			}
		}
	},
	
	// 关闭界面
	closeButton : function(sender, type) {
		
		if (type === ccui.Widget.TOUCH_BEGAN) {
			UIManager.closeUILayer(UILayer.CompositeLayer);
		}
	},
	
	onEnter : function() {
		this._super();
	
	},

	onEnterTransitionDidFinish : function() {
		this._super();
		
	},

	onExitTransitionDidFinish : function() {
		this._super();
		
	},

	onExit : function() {
		this._super();
	
	},

});