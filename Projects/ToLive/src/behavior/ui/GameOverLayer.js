//游戏结束界面
var GameOverLayer = cc.Layer.extend({


	restartButton : null, 
	exitButton  : null,
	overLabel : null,

	ctor : function () {

		var widget;
		this._super();
		widget = ccs.uiReader.widgetFromJsonFile(res.GameOverLayer);

		// 添加按钮
		this.restartButton = widget.getChildByNameRecursively("RestartButton");
		this.exitButton  = widget.getChildByNameRecursively("ExitButton");
		this.restartButton.addTouchEventListener(this.restartButtonTouch.bind(this), this);
		this.exitButton.addTouchEventListener(this.exitButtonTouch.bind(this), this);
		
		// 获取文本
		this.overLabel = widget.getChildByNameRecursively("OverLabel");
		this.overLabel.setString("你总共生存了"+(StayLiveDays + 1)+"天");
		this.addChild(widget);
	},

	// 重新开始按钮按下
	restartButtonTouch : function(sender, type) {

		if (type === ccui.Widget.TOUCH_BEGAN) {
			var gameScene = new GameScene(true);
			cc.director.replaceScene(gameScene);
		}
	},

	// 退出按钮按下
	exitButtonTouch : function(sender, type) {

		if (type === ccui.Widget.TOUCH_BEGAN) {
			cc.director.end();
		}
	},

	onEnter : function() {
		this._super();
		
	},

	onEnterTransitionDidFinish : function() {
		this._super();
		
	},

	onExitTransitionDidFinish : function() {
		this._super();
		
	},

	onExit : function() {
		this._super();
		
	},

});