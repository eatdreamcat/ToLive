// 自定义地图类
/*
 * 坐标系跟cocos坐标系统一，左下角为原点
 */
var TLMap = cc.Node.extend({
	
	mapSize : null, // 地图大小(图块数量)
	tileSize : null, // 图块大小
	layers : null, // 所有的图层
	
	// 默认初始化方式
	ctor : function () {
		
		this._super();
		this.layers = [];
		this.mapSize = cc.size(mapDesignSize.width, mapDesignSize.height);
		this.tileSize = cc.size(tileDesignSize.width, tileDesignSize.height);
		this.setAnchorPoint(0, 0);
		
	},
	
	// 获取mapSize
	getMapSize : function() {
		return this.mapSize;
	},
	
	// 获取tileSize
	getTileSize : function () {
		return this.tileSize;
	},
	
	// 设置图层
	setLayer : function(layer, layerZOrder) {
		
		this.layers[layer.getName()] = layer;
		layer.setPosition(0, 0);
		this.addChild(layer.getLayerNode(), layerZOrder);
	},
	// 根据name获取Layer
	getLayer : function (name) {
		return  this.layers[name];
	},
});