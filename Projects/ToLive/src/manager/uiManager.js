/**
 *界面管理器 
 */
var UIManager = {
	
		// 显示某个界面
		showUILayer : function (layer) {
			
			var uiLayer;
			
			uiLayer = this.getUILayerByName(layer.name);
			if (uiLayer == null) {
				var currentScene = cc.director.getRunningScene();
				var s = "new " + layer.name + "()"
				uiLayer = eval(s);
				uiLayer.setName(layer.name);
				currentScene.addChild(uiLayer, layer.zOrder);
			}
			uiLayer.setVisible(true);
			return uiLayer;
		},
		
		// 关闭某个界面
		closeUILayer : function (layer) {
			
			var uiLayer = this.getUILayerByName(layer.name);
			if (uiLayer != null) {
				uiLayer.setVisible(false);
			}
		},
		
		// 获取某个界面
		getUILayerByName : function (name) {
			
			var currentScene = cc.director.getRunningScene();
			return currentScene.getChildByName(name);
		},
		
};