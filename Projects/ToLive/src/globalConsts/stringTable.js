// 定义一些常量字符串
var stringTable = {
		To_Find_Empty_Place : "这块地已经种不下这么多植物了",
		Has_No_Catalpa : "装备铁楸来挖掘吧",
		Has_No_Headchet : "装备了斧头才能砍树",
		Has_No_Pricker : "装备了凿子才能凿开石头",
		Night_Without_Fire : ["我害怕黑夜","我感觉我要精神崩溃了","怎么什么都看不到，我要死了"],
};