/**
 * 
 游戏玩家脚本
*/
var PlayerNode = cc.Node.extend({
	
	mapPosition : null, // 地图中的坐标
	moveSpeed : Speed.PlayerSpeed, // 移动的速度(-2)
	state : null, // 玩家状态 （移动，空闲，死亡等...）
	weapon : null, // 玩家武器
	// 数值
	currentHealth : 100,
	currentHungry : 100,
	currentSanity : 100,
	
	maxHealth : 100,
	maxHungry : 100,
	maxSanity : 100,
	
	// 动画
	playerArmature : null, // 角色动画 
	animation : null,
	armatureAnimation : null,
	
	// 骨骼
	weaponHand : null,
	
	// 道具相关
	tools : {}, // 玩家所拥有的道具
	
	ctor : function () {
		
		this._super();
		this.tools = {};
		
		/*
		 * 测试
		 */
		this.tools[16] = {
				number:1,
				health:100,
				isEquip:false,
		};
		this.tools[17] = {
				number:1,
				health:100,
				isEquip:false,
		};
		this.tools[27] = {
				number:10,
				health:1000,
				isEquip:false,
		};
		this.tools[22] = {
				number:1,
				health:100,
				isEquip:false,
		};
		this.tools[24] = {
				number:100,
				health:1,
				isEquip:false,
		};
		/*
		 * 测试
		 */
		
		this.state = PlayerState.Idle;
		this.weapon = {
				equip : PlayerWeapon.noWeapon,
				weaponId : -1,
				};
		 // 初始化动画
		ccs.armatureDataManager.addArmatureFileInfo(res.playerAnimation);
		this.playerArmature = new ccs.Armature(ArmatureDataName.Player);
		this.weaponHand = this.playerArmature.getBone("weaponHand");
		this.playerArmature.setAnchorPoint(0.5, 0);
		this.playerArmature.setName("armature");
		this.addChild(this.playerArmature);
		this.playArmatureByState();
		
	},
	
	/**
	 * 道具相关
	 * @returns {___anonymous411_412}
	 */

	// 获取玩家所有道具
	getAllTools : function() {
	    return this.tools;	
	},
	
	// 设置装备持久度
	setWeaponHealth : function(weaponId, cost) {
	
		if (this.tools[weaponId] != null) {
			this.tools[weaponId].health-=cost;
		}
	},
	
	// 添加道具
	addToolById : function(id, number) {

		var living = LivingConfig.getItemById(id);
		if (this.tools[id] != null) {
			this.tools[id].number += number;
			this.tools[id].health = this.tools[id].health + living.health*number;
		} else {
			this.tools[id] = {
					number : number,
					health : living.health*number,
					isEquip:false,
			};
		}
		
		// 派发事件
		var event = new cc.EventCustom(EventName.addGoodsEvent);
		cc.eventManager.dispatchEvent(event);
	},

	//移除道具
	removeToolById : function(id, number) {

		if (this.tools[id] != null) {
			if (this.tools[id].number <= number) {
				this.tools[id] = null;
				delete this.tools[id];
			} else {
				this.tools[id].number -= number;
			}
		} 
	},

	// 根据id获取道具数量
	getToolById : function(id) {

		if (this.tools[id] == null) {
			return {number : 0};
		}
		return this.tools[id];
	},
	
	/**
	 * 动画相关
	 */
	// 根绝角色状态播放角色动画
	playArmatureByState : function() {
		
		if (this.state == null) {
			this.state = PlayerState.Idle;
		}
		
		// 判断动画数据是否存在
		var animation = this.playerArmature.getAnimation();
		if (animation.getAnimationData().getMovement(this.state) != null) {
			animation.play(this.state);
		}
	},
	
	/**
	 * 状态相关
	 * @param _state
	 */
	// 设置角色当前状态
	setCurrentState : function(_state) {
		
		// 状态改变，更新动画显示
		if (this.state !== _state) {
			this.state = _state;
			this.playArmatureByState();
		}
		
		
	},
	
	// 获取角色当前状态
	getCurrentState : function() {
	    return this.state;
	},
	
	/**
	 * 武器相关
	 * @param foodID
	 */
    equipWeapon : function (_weapon, id) {
    	
    	var weaponNode, currentWeapon, weaponSkin;
    	var weaponConfig = LivingConfig.getItemById(id);
    	
    	currentWeapon = this.getCurrentWeapon();
    	
    	if (currentWeapon.equip !== PlayerWeapon.noWeapon) {
    		this.getToolById(currentWeapon.weaponId).isEquip = false;
    	} 
    	
    	// 更新贴图
    	weaponSkin = new ccui.ImageView();
    	weaponSkin.loadTexture(weaponConfig.pic, ccui.Widget.PLIST_TEXTURE);
    	weaponSkin.setAnchorPoint(0, 0);
        this.weaponHand.addDisplay(weaponSkin, 0);
        this.weaponHand.changeDisplayWithIndex(0, true);
        
    	this.getToolById(id).isEquip = true;
    	this.weapon.equip = _weapon;
    	this.weapon.weaponId = id;

    	var event = new cc.EventCustom(EventName.playerEquipWeaponEvent);
    	cc.eventManager.dispatchEvent(event);
    },
    
    removeWeapon : function() {
    	
		if (this.weapon.equip !== PlayerWeapon.noWeapon) {
			this.tools[this.weapon.weaponId].isEquip = false;
			// 移除贴图
			this.weaponHand.removeDisplay(0);
		}
		this.weapon.equip = PlayerWeapon.noWeapon;
		this.weapon.weaponId = -1;
		
		var event = new cc.EventCustom(EventName.playerEquipWeaponEvent);
		cc.eventManager.dispatchEvent(event);
	},
	
	getCurrentWeapon : function() {
	    return this.weapon;	
	},
	
	// 吃东西
	eat : function (foodID) {
		
	},
	
	// 处理东西（捡东西，砍树..）
	doSomething : function (ID) {
		
	},
	
	/**
	 * 数据相关
	 * @returns {Number}
	 */
	// 获取当前生命值
	getCurrentHealth : function () {
		return this.currentHealth;
	},
	
	// 获取当前饥饿值
	getCurrentHungry : function() {
		return this.currentHungry;
	},
	
	// 获取当前精神值
	getCurrentSanity : function() {
		return this.currentSanity;
	},
	
	// 设置生命值
	setCurrentHealth : function (number) {
		
		// 精神值影响健康值
		if (number > 0) {
			number = (this.currentSanity / this.maxSanity) * number;
		} else {
			number = (this.maxSanity / this.currentSanity) * number;
		}
		this.currentHealth += number;
		
		// 健康值低于50％，精神值受损
		if (this.currentHealth < this.maxHealth / 2) {
			this.setCurrentSanity(number);
		}
		
		if (this.currentHealth > this.maxHealth) {
			this.currentHealth = this.maxHealth;
		} else if (this.currentHealth <= 0) {
			
			// 派发事件
			this.currentHealth = 0;
			var event = new cc.EventCustom(EventName.playerDie);
			cc.eventManager.dispatchEvent(event);
		}
		
	},
	
	// 设置饥饿值
	setCurrentHungry : function (number) {
		
		// 精神值影响饥饿值
		if (number > 0) {
			number = (this.currentSanity / this.maxSanity) * number;
		} else {
			number = (this.maxSanity / this.currentSanity) * number;
		}
		this.currentHungry += number;
		
		// 饥饿值低于50％，健康值也受损
		if (this.currentHungry < this.maxHungry / 2) {
			this.setCurrentHealth(number);
		}
		
		// 饥饿值低于30%，精神值受损
		if (this.currentHungry < this.maxHungry / 3) {
			this.setCurrentSanity(number);
		}
		
		if (this.currentHungry > this.maxHungry) {
			this.currentHungry = this.maxHungry;
		} else if (this.currentHungry <= 0) {
			// 派发事件
			this.currentHungry = 0;
			var event = new cc.EventCustom(EventName.playerDie);
			cc.eventManager.dispatchEvent(event);
		}
		
	},
	
	// 设置精神值
	setCurrentSanity : function (number) {
		
		// 健康值影响精神值
		if (number > 0) {
			number = (this.currentHealth / this.maxHealth) * number;
		} else {
			number = (this.maxHealth / this.currentHealth) * number;
		}
		this.currentSanity += number;
		
		if (this.currentSanity > this.maxSanity) {
			this.currentSanity = this.maxSanity;
		} else if (this.currentSanity <= 0) {
			// 派发事件
			this.currentSanity = 0;
			var event = new cc.EventCustom(EventName.playerDie);
			cc.eventManager.dispatchEvent(event);
		}
	},
	
	// 获取当前生命值
	getMaxHealth : function () {
		return this.maxHealth;
	},

	// 获取当前饥饿值
	getMaxtHungry : function() {
		return this.maxHungry;
	},

	// 获取当前精神值
	getMaxSanity : function() {
		return this.maxSanity;
	},

	// 设置生命值
	setMaxHealth : function (number) {
		this.maxHealth = health;
	},

	// 设置饥饿值
	setMaxHungry : function (number) {
		this.maxHungry = number;
	},

	// 设置精神值
	setMaxSanity : function (number) {
		this.maxSanity = number;
	},
	
	// 获取移动速度
	getSpeed : function() {
		return this.moveSpeed;
	},
});