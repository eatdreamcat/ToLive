attribute vec4 sprite_position;
attribute vec4 sprite_color;

varying vec4 v_fragmentColor;

void main()
{
    gl_Position = CC_MVPMatrix * sprite_position;
    v_fragmentColor = sprite_color;
}